import os
from typing import TYPE_CHECKING

from nawah.classes import Attr, Func, Module, Package, Perm, Var
from nawah.utils import validate_attr, var_value
from unifonicnextgen.controllers.rest_controller import RestController

if TYPE_CHECKING:
    from nawah.types import NawahDoc, Results


__version__ = '0.0.0'
with open(
    os.path.join(os.path.dirname(__file__), 'version.txt'), encoding='UTF-8'
) as f:
    __version__ = f.read().strip()

unifonic_auth_type = Attr.TYPED_DICT(
    dict={
        'sid': Attr.STR(),
        'sender': Attr.STR(),
    }
)


async def _send_sms(
    doc: 'NawahDoc',
) -> 'Results':
    phone = doc['phone']
    content = doc['content']

    unifonic_auth = None

    if 'unifonic_auth' in doc:
        validate_attr(
            mode='create',
            attr_name='unifonic_auth',
            attr_type=unifonic_auth_type,
            attr_val=doc['unifonic_auth'],
        )
        unifonic_auth = doc['unifonic_auth']

    if not unifonic_auth:
        unifonic_auth = var_value(Var.CONFIG('unifonic'))

    RestController().create_send_message(
        app_sid=unifonic_auth['sid'],
        sender_id=unifonic_auth['sender'],
        body=content,
        recipient=phone,
    )

    return {
        'status': 200,
        'msg': 'Request to send message is successful',
        'args': {},
    }


unifonic = Module(
    name='unifonic',
    funcs={
        'send_sms': Func(
            permissions=[Perm(privilege='__sys')],
            doc_attrs={
                'phone': Attr.PHONE(),
                'content': Attr.STR(),
            },
            callable=_send_sms,
        )
    },
)


nawah_unifonic = Package(
    name='nawah_unifonic',
    api_level='2.0',
    version=__version__,
    vars_types={
        'unifonic': Attr.TYPED_DICT(dict={'sid': Attr.STR(), 'sender': Attr.STR()})
    },
    modules=[unifonic],
)
