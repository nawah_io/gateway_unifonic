import os.path

import setuptools


def _file_abs_path(*path: str) -> str:
    return os.path.join(os.path.dirname(__file__), *path)


with open(_file_abs_path('nawah_unifonic', 'version.txt'), encoding='UTF-8') as f:
    __version__ = f.read().strip()

with open(_file_abs_path('README.md'), 'r', encoding='UTF-8') as f:
    long_description = f.read()

with open(_file_abs_path('requirements.txt'), 'r', encoding='UTF-8') as f:
    requirements = f.readlines()

with open(_file_abs_path('dev_requirements.txt'), 'r', encoding='UTF-8') as f:
    dev_requirements = f.readlines()

setuptools.setup(
    name='nawah_unifonic',
    version=__version__,
    author='Mahmoud Abduljawad',
    author_email='mahmoud@masaar.com',
    description='Nawah package for Unifonic SMS',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/nawah_io/nawah_unifonic',
    package_data={
        'nawah_unifonic': ['version.txt'],
    },
    packages=[
        'nawah_unifonic',
    ],
    project_urls={
        'Docs: Gitlab': 'https://gitlab.com/nawah_io/nawah_unifonic',
        'Gitlab: issues': 'https://gitlab.com/nawah_io/nawah_unifonic/-/issues',
        'Gitlab: repo': 'https://gitlab.com/nawah_io/nawah_unifonic',
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Topic :: Internet :: WWW/HTTP',
        'Framework :: AsyncIO',
    ],
    python_requires='>=3.10.2',
    install_requires=requirements,
    extras_require={'dev': dev_requirements},
)
